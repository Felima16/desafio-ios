//
//  GameCell.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import UIKit

class GameCell: UICollectionViewCell {
    
    @IBOutlet weak var nameGameLabel: UILabel!
    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var gameImageView: UIImageView!
    
}
