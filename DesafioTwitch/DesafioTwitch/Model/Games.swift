//
//  Games.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import AlamofireObjectMapper
import ObjectMapper
import Foundation

class AllGames: Mappable {
    
    var total: Int?
    var top: [Top]?
    
    required init?(map: Map){
        mapping(map: map)
    }
    
    func mapping(map:Map){
        total     <- map["_total"]
        top       <- map["top"]
    }
}

class Top: Mappable {
    
    var viewers: Int?
    var game: Game?
    
    required init?(map: Map){
        mapping(map: map)
    }
    
    func mapping(map:Map){
        viewers     <- map["viewers"]
        game        <- map["game"]
    }
}


class Game: Mappable {
    
    var name: String?
    var popularity: Int?
    var logo: Logo?
    
    required init?(map: Map){
        mapping(map: map)
    }
    
    func mapping(map:Map){
        name            <- map["name"]
        popularity      <- map["popularity"]
        logo            <- map["logo"]
        
    }
    
}



class Logo: Mappable {
    
    var large: String?
    var small: String?
    
    required init?(map: Map){
        mapping(map: map)
    }
    
    func mapping(map:Map){
        large    <- map["large"]
        small    <- map["small"]
        
    }
    
}

