//
//  ModelManager.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import Foundation


import UIKit

let mm = ModelManager.instance
class ModelManager: NSObject {
    
    //Singleton struct
    static let instance = ModelManager()
    
    //representaçoes das models
    var allGames: AllGames?
    var page = 0
    
    func maxPages() -> Bool{
        if let total = allGames?.total{
            if total%10 == 0{
                if total/10 > page{
                    return true
                }
            }else{
                if total/10 >= page{
                    return true
                }
            }
        }
        
        return false
    }
}
