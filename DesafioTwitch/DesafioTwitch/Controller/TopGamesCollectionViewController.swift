//
//  TopGamesCollectionViewController.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import UIKit

private let reuseIdentifier = "GamesCell"

class TopGamesCollectionViewController: UICollectionViewController {
    
    fileprivate let itemSpacing:CGFloat = 10
    fileprivate var thumbnailSize:CGSize = CGSize.zero

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        loadCollection()
    }
    
    private func loadCollection(){
        let para = ["offset":"\(mm.page * 10)"]
        TwitchAPI.get(AllGames.self, url: "https://api.twitch.tv/kraken/games/top", parameters: para, finish: {
            print("carregou")
        }, success: { (item) in
            if mm.page == 0{
                mm.allGames = item
                mm.page += 1
            }else{
                mm.allGames?.top?.append(contentsOf: item.top!)
                mm.page += 1
            }
            self.collectionView?.reloadData()
        }) { (error, code) in
            print("--- ERROR \(error.localizedDescription) --- CODE \(code ?? 0)")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == (mm.allGames?.top?.count)! - 1 ) { //it's your last cell
            if mm.maxPages(){
                loadCollection()
            }
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mm.allGames?.top?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GameCell
        cell.nameGameLabel.text = mm.allGames?.top![indexPath.row].game?.name
        cell.rankingLabel.text = "\(mm.allGames?.top![indexPath.row].game?.popularity ?? 0)"
        
        TwitchAPI.getImage(url: (mm.allGames?.top![indexPath.row].game?.logo?.small)!, success: { (image) in
            cell.gameImageView.image = image
        }) { (error) in
             print("--- ERROR \(error.localizedDescription) ")
        }
        
        cell.contentView.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        cell.contentView.layer.cornerRadius = 6
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderWidth = 0.1
        cell.contentView.layer.borderColor = UIColor.black.cgColor
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = storyboard?.instantiateViewController(withIdentifier: "DetailsVireController") as! DetailsViewController
        detail.index = indexPath.row
        self.navigationController?.pushViewController(detail, animated: true)
    }

    // MARK: UICollectionViewDelegate

}

extension TopGamesCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //print("Called collectionView collectionViewLayout sizeForItemAt")
        // Calculate thumbnail size based on device
        if traitCollection.userInterfaceIdiom == .pad {
            //print("iPad")
            if UIDevice.current.orientation.isLandscape {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 5) / 4)
            } else {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 4) / 3)
            }
        } else if traitCollection.userInterfaceIdiom == .phone {
            //print("iPhone")
            thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 3) / 2)
        }
        thumbnailSize.height = thumbnailSize.width * 1.15
        return thumbnailSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //print("Called collectionView collectionViewLayout insetForSectionAt")
        return UIEdgeInsets(top: itemSpacing, left: itemSpacing, bottom: itemSpacing, right: itemSpacing)
    }
    
}
