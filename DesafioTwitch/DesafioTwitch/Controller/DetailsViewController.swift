//
//  ViewController.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var gameImage: UIImageView!
    
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    private func initSetup(){
        nameLabel.text = mm.allGames?.top![index].game?.name
        viewsLabel.text = "\(mm.allGames?.top![index].viewers ?? 0)"
        TwitchAPI.getImage(url: (mm.allGames?.top![index].game?.logo?.large)!, success: { (image) in
            self.gameImage.image = image
        }) { (error) in
            print("--- ERROR \(error.localizedDescription) ")
        }
    }

}

