//
//  TwitchAPI.swift
//  DesafioTwitch
//
//  Created by Fernanda de Lima on 12/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import AlamofireImage
import Alamofire
import ObjectMapper

class TwitchAPI {
    
    //cria o get pegando as infomaçoes mapeando qualquer tipo de classe mappable
    static func get <T: Any>
        (_ type: T.Type,
         url: String,
         parameters: Parameters,
         finish:@escaping () -> Void,
         success:@escaping (_ item: T) -> Void,
         fail:@escaping (_ error: Error,_ code:Int?) -> Void) -> Void where T:Mappable {

        let headers: HTTPHeaders = [
            "Client-ID": "eysmw5dakudciaxgsmeo88sej9y5mw"
        ]
        
        Alamofire.request(url,
                          parameters : parameters,
                          encoding: URLEncoding(destination: .queryString),
                          headers: headers).responseObject { (response: DataResponse<T>) in
            print("GET ------ \(url) with status code \(response.response?.statusCode ?? 0)")
            if response.response?.statusCode == 404{
                finish()
            }else{
                switch response.result {
                case .success(let item):
                    print("Response from GET ---- \(String(describing: response.result.value))")
                    success(item)
                case .failure(let error):
                    print("ERROR GET ---- ", error)
                    fail(error, (response.response?.statusCode)!)
                }
            }
        }
    }
    
    
    //get de imagens 
    static func getImage(url: String,
                         success: @escaping (_ img: Image) -> Void,
                         fail:@escaping (_ error: Error) ->Void){
        
        Alamofire.request(url).responseImage { response in
            switch response.result{
            case .success:
                print("Response from IMAGE ---- \(response.result.value)")
                if let img = response.result.value{
                    success(img)
                }
            case .failure(let error):
                print("ERROR POST ---- ", error)
                fail(error)
            }
        }
    }
}
